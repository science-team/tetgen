#include <iostream>
#include "tetgen.h"

int main(int argc, char **argv)
{
    int n;
    tetgenio in;
    in.initialize();
    tetgenio ou;
    ou.initialize();

    in.numberofpoints = 8;
    in.pointlist = new REAL[in.numberofpoints * 3];

    n = 0;
    in.pointlist[3 * n + 0] = 0.0;
    in.pointlist[3 * n + 1] = 0.0;
    in.pointlist[3 * n + 2] = 0.0;

    n = 1;
    in.pointlist[3 * n + 0] = 1.0;
    in.pointlist[3 * n + 1] = 0.0;
    in.pointlist[3 * n + 2] = 0.0;

    n = 2;
    in.pointlist[3 * n + 0] = 1.0;
    in.pointlist[3 * n + 1] = 1.0;
    in.pointlist[3 * n + 2] = 0.0;

    n = 3;
    in.pointlist[3 * n + 0] = 0.0;
    in.pointlist[3 * n + 1] = 1.0;
    in.pointlist[3 * n + 2] = 0.0;

    n = 4;
    in.pointlist[3 * n + 0] = 0.0;
    in.pointlist[3 * n + 1] = 0.0;
    in.pointlist[3 * n + 2] = 1.0;

    n = 5;
    in.pointlist[3 * n + 0] = 1.0;
    in.pointlist[3 * n + 1] = 0.0;
    in.pointlist[3 * n + 2] = 1.0;

    n = 6;
    in.pointlist[3 * n + 0] = 1.0;
    in.pointlist[3 * n + 1] = 1.0;
    in.pointlist[3 * n + 2] = 1.0;

    n = 7;
    in.pointlist[3 * n + 0] = 0.0;
    in.pointlist[3 * n + 1] = 1.0;
    in.pointlist[3 * n + 2] = 1.0;

    tetrahedralize((char *)("ev"), &in, &ou);

    std::cout << "Number of points: " << ou.numberofpoints << std::endl;
    std::cout << "Number of edges: " << ou.numberofedges << std::endl;
    std::cout << "Number of trifaces: " << ou.numberoftrifaces << std::endl;
    std::cout << "Number of tetrahedra: " << ou.numberoftetrahedra << std::endl;
    std::cout << "Number of corners: " << ou.numberofcorners << std::endl;
    std::cout << "List of edges:" << std::endl;
    for (int n = 0; n < ou.numberofedges; ++n)
    {
        const auto n0 = ou.edgelist[2 * n + 0];
        const auto n1 = ou.edgelist[2 * n + 1];
        const auto *u = &ou.pointlist[3 * n0];
        const auto *v = &ou.pointlist[3 * n1];
        std::cout << "[" << u[0] << " ; " << u[1] << " ; " << u[2] << "] - [" << v[0] << " ; " << v[1] << " ; " << v[2] << "]" << std::endl;
    }

    std::cout << "Number of vpoints: " << ou.numberofvpoints << std::endl;
    std::cout << "Number of vedges: " << ou.numberofvedges << std::endl;
    std::cout << "Number of vfacets: " << ou.numberofvfacets << std::endl;
    std::cout << "Number of vcells: " << ou.numberofvcells << std::endl;
    std::cout << "List of edges:" << std::endl;
    for (int n = 0; n < ou.numberofvedges; ++n)
    {
        const auto e = ou.vedgelist[n];
        const auto n0 = e.v1;
        const auto n1 = e.v2;
        const auto *u = &ou.vpointlist[3 * n0];
        const auto *v = n1 == -1 ? e.vnormal : &ou.vpointlist[3 * n1];
        std::cout << "[" << u[0] << " ; " << u[1] << " ; " << u[2] << "] - [" << v[0] << " ; " << v[1] << " ; " << v[2] << "]" << std::endl;
    }
    return 0;
}
